﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MVC5_First;
using MVC5_First.Controllers;
using System.Web.Mvc;

namespace MVC5_First.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("Home Page", result.ViewBag.Title);
        }
    }
}
