﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.Web;
using System.Web.Http.Filters;
using System.Web.Http;

namespace MVC5_First.Middleware.Filter
{
    public class HostAuthenticationFilterCustom : HostAuthenticationFilter
    {
        public HostAuthenticationFilterCustom(string authenticationType) : base(authenticationType)
        {
        }
        
        public new Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            var con = context;
            var err = con.ErrorResult;
            return base.AuthenticateAsync(context, cancellationToken);
        }

        public new Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            return base.ChallengeAsync(context, cancellationToken);
        }
    }
}