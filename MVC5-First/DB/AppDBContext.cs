﻿using MVC5_First.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MVC5_First.DB
{
    public class AppDBContext : DbContext
    {
        public AppDBContext() { }
        public DbSet<Product> Products { get; set; }
    }
}