﻿using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MVC5_First.Models
{
    public class Product
    {
        //This is wrong name, I need to overwrite it sadly
        [Column(name: "id")]
        public int Id { get; set; }
        public string ProductName { get; set; }
        public int Price { get; set; }
        public int Stock { get; set; }
    }
}