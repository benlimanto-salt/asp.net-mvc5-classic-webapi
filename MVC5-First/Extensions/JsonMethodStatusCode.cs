﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace MVC5_First.Extensions
{
    public static class JsonMethodStatusCode
    {

        public static JsonResult Json(this Controller controller, object obj, HttpStatusCode statusCode = HttpStatusCode.OK)
        {
            controller.Response.StatusCode = (int)statusCode;
            return new JsonResult()
            {
                Data = obj,
                ContentType = "application/json",
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public static HttpResponseMessage Json(this ApiController controller, object obj, HttpStatusCode statusCode = HttpStatusCode.OK)
        {
            var result = new JsonResult()
            {
                Data = obj,
                ContentType = "application/json",
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

            return controller.Request.CreateResponse(statusCode, result);
        }
    }
}