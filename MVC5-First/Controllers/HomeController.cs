﻿using MVC5_First.DB;
using MVC5_First.Models;
using MVC5_First.Models.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using FromBody = System.Web.Http.FromBodyAttribute;
using BindAttribute =  System.Web.Mvc.BindAttribute;
using CAuthorize = MVC5_First.Middleware.Filter.CustomAuthorize;
using System.Net;
using System.Web.Http;

namespace MVC5_First.Controllers
{
    // For WebAPI we really need... ApiController...
    public class HomeController : ApiController
    {
        private AppDBContext _db;
        public HomeController() {
            // There are no DI for... .NET 4.5
            // Making Testing the Controller harder
            _db = new AppDBContext();
        }
        // SO Sad need manually change authorize.. can't just adding any transient for it like in .NET Core ...
        // @see https://stackoverflow.com/a/42506624/4906348
        [CAuthorize]
        [Route("product"), HttpGet]
        public IHttpActionResult GetData()
        {
            var d = _db.Products.ToList();
            var u = User.Identity.IsAuthenticated;
            var user = (System.Security.Claims.ClaimsIdentity)User.Identity;
            var username = user.Claims.Where(q => q.Type == "name").First().Value;
            // Allow Get need to be passed manually :/ 
            return Json(new { product = d, auth = u, username });
        }

        [Route("product"), HttpPost]
        public IHttpActionResult ProductInsert([FromBody, Bind(Exclude = "Id")] Product request)
        {
            var d = new Product()
            {
                ProductName = request.ProductName,
                Price = request.Price,
                Stock = request.Stock,
            };

            _db.Products.Add(d);
            _db.SaveChanges();

            return Json(new ApiResponse() { 
                Message = "Success insert product"
            });
        }

    }
}
