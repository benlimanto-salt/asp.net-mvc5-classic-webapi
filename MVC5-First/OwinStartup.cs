﻿using Microsoft.IdentityModel.Tokens;
using Microsoft.Owin;
using Microsoft.Owin.Security.Jwt;
using Owin;
using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin.Security;

[assembly: OwinStartup(typeof(MVC5_First.OwinStartup))]

namespace MVC5_First
{
    public class OwinStartup
    {
        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888
            // @see https://www.c-sharpcorner.com/article/asp-net-web-api-2-creating-and-validating-jwt-json-web-token/
            app.UseJwtBearerAuthentication(
                new JwtBearerAuthenticationOptions
                {
                    AuthenticationMode = AuthenticationMode.Active,
                    TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = "http://192.168.122.85", //some string, normally web url,  
                        ValidAudience = "http://192.168.122.85",
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("asp.net_mvc5_salt"))
                    }
                });
        }
    }
}
