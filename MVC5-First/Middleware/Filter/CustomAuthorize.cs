﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace MVC5_First.Middleware.Filter
{
    /// <summary>
    /// Sadly there are no way to respond with custom response like in 
    /// .NET Core... 
    /// </summary>
    public class CustomAuthorize : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            if (actionContext == null)
            {
                throw new ArgumentException("actionContext not passed");
            }

            actionContext.Response = actionContext.ControllerContext.Request.CreateResponse(HttpStatusCode.Unauthorized, new {
                code = 401,
                message = "WHO ARE YOU?"
            });
        }
    }
}