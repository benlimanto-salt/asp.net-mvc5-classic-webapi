﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
//using System.Web.Mvc;
using System.Net;
using MVC5_First.Extensions;
using MVC5_First.Middleware.Filter;
using System.Web.Http;
using System.Net.Http;

namespace MVC5_First.Controllers
{
    [RoutePrefix("http")]
    public class HttpPlusAuthController : System.Web.Http.ApiController
    {
        // GET: HttpPlusAuth
        [Route("404")]
        public HttpResponseMessage Index()
        {
            // To use extension sadly we need this... 
            // I don't know why
            return this.Json(new { msg = "404" }, HttpStatusCode.NotFound);
            //return this.Json(new)
        }

        [Route("403")]
        public HttpResponseMessage ForbiddenNormal()
        {
            //Response.StatusCode = 403;
            // To use extension sadly we need this... 
            // I don't know why
            return this.Json(new { msg = "403" }, HttpStatusCode.Forbidden);
        }

        [Route("201")]
        public HttpResponseMessage Created()
        {
            // To use extension sadly we need this... 
            // I don't know why
            return this.Json(new { msg = "201" }, HttpStatusCode.Created);
        }

        [Route("login"), HttpPost]
        public object Login()
        {
            if (Request.Headers.GetValues("User") == null)
                return this.Json(new { msg = "Bad Request" }, HttpStatusCode.BadRequest);

            string user = Request.Headers.Where(q => q.Key == "User").First().Value.ToString();
            
            string key = "asp.net_mvc5_salt"; //Secret key which will be used later during validation    
            var issuer = "http://192.168.122.85";  //normally this will be your site URL    

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            //Create a List of Claims, Keep claims name short    
            var permClaims = new List<Claim>();
            permClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
            permClaims.Add(new Claim("valid", "1"));
            permClaims.Add(new Claim("userid", "1"));
            permClaims.Add(new Claim("name", user));

            //Create Security Token object by giving required parameters    
            var token = new JwtSecurityToken(issuer, //Issure    
                            issuer,  //Audience    
                            permClaims,
                            expires: DateTime.Now.AddDays(1),
                            signingCredentials: credentials);
            var jwt_token = new JwtSecurityTokenHandler().WriteToken(token);

            return this.Json(new { token = jwt_token });
        }

        [Route("user"), HttpGet, CustomAuthorize]
        public object GetUser()
        {
            var user = (System.Security.Claims.ClaimsIdentity)User.Identity;
            var list = user.Claims.ToDictionary(k => k.Type, k => k.Value);
            return this.Json(new
            {
                user = list,
            });
        }
    }
}