﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC5_First.Models.Responses
{
    public class ApiResponse
    {
        [JsonProperty("success")]
        public bool Success { get; set; } = false;
        [JsonProperty("code")]
        public int Code { get; set; }
        [JsonProperty("messages")]
        public string Message { get; set; }
        [JsonProperty("data")]
        public object Data { get; set; }
    }
}